import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AuthGuard } from './guards';
import { AuthInterceptor, LoaderInterceptor } from './interceptors';
import { AuthService } from './services';



@NgModule({
  declarations: [],
  imports: [HttpClientModule],
  providers:[
    AuthService,
    AuthGuard,
    {provide:HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true},
    {provide:HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
  ]
})
export class CoreModule { }
