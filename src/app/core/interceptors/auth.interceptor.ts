import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, finalize, Observable, throwError } from 'rxjs';
import { AuthService, LoaderService } from '../services';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService:AuthService, private loaderService:LoaderService) {}

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const token = this.authService.getToken();

    if(token) {
      req = req.clone({ 
        headers: req.headers.append('Authorization', `Bearer ${token}`) 
      });
    }
    
    return next.handle(req).pipe(
      catchError((err:HttpErrorResponse) => {
        console.error('ERROR AUTH', err);
        if (err.status === 403) alert('Introduce un usuario y contraseña valida.');
        this.authService.logout(); 
        return throwError(() => 'error auth interceptor');
      })
    );
  }

}

