import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map, Observable } from 'rxjs';
import { AuthUser } from 'src/app/login/authUser.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthService {

  url = environment.api.baseUrl;

  constructor(private http:HttpClient, private router:Router) {}

  public login(body:AuthUser):Observable<any>{
    return this.http.post(`${this.url}/authenticate`,body).pipe(
      map((res:any) => {
        localStorage.setItem('token', res.jwt);          
        return res;
      })
    );
  }

  public getToken(): string | null {
    return localStorage.getItem('token');
  }

  public logout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }

  public existsToken(): boolean {
    const token = localStorage.getItem('token');
    return !!token;
  }

}
