import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { SharedComponentsModule } from '../_shared/shared-components.module';



@NgModule({
  declarations: [LoginComponent],
  imports: [
    SharedComponentsModule,
  ]
})
export class LoginModule { }
