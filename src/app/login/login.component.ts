import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../core/services';
import { AuthUser } from './authUser.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isLoading:boolean = false;

  constructor(private authService:AuthService,
    private router:Router) { }

  ngOnInit(): void {
    if (this.authService.existsToken()) this.router.navigate(['articles']);
  }

  onLogin(form:any) {
    const user:AuthUser = form.value;
    this.isLoading = true;
    this.authService.login(user).subscribe(
      res =>{
      this.isLoading = false;
      this.router.navigate(['articles']);
    },
      error => {
        console.error(error);
        this.isLoading = false;
      }
    );
  }

}
