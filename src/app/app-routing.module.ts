import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards';
import { LoginComponent } from './login/login.component';
import { ArticleFormComponent } from './pages/article/article-form/article-form.component';
import { ArticleListComponent } from './pages/article/article-list/article-list.component';
import { SupplierComponent } from './supplier/supplier.component';

const routes: Routes = [
  {path:'articles', component: ArticleListComponent, canActivate:[AuthGuard]},
  {path:'suppliers', component: SupplierComponent},
  {path:'articles/form', component: ArticleFormComponent},
  {path:'articles/form/:articleCode', component: ArticleFormComponent},
  {path:'**', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
