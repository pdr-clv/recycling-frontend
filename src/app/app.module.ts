import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SupplierComponent } from './supplier/supplier.component';
import { ArticleModule } from './pages/article/article.module';
import { SharedComponentsModule } from './_shared/shared-components.module';
import { LoginModule } from './login/login.module';
import { CoreModule } from './core/core.module';
import { AuthService } from './core/services';

@NgModule({
  declarations: [
    AppComponent,
    SupplierComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ArticleModule,
    SharedComponentsModule,
    CoreModule,
    LoginModule
  ],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
