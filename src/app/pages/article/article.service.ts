import { Injectable } from '@angular/core';
import { Article } from './article.model';
import { HttpClient } from'@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ArticleService {

  private articles: Article[] = [];
  url: string = environment.api.baseUrl;
  articleUrl: string = `${this.url}/articles`

  constructor(private http: HttpClient) { }

  public getList(filter?: string): Observable<Article[]> {
    let filterUrl = this.articleUrl;
    if (!!filter) filterUrl = `${filterUrl}?filter=${filter}`;
    return this.http.get<Article[]>(filterUrl).pipe(catchError(this.errorHandl));
  }

  public getByCode(articleCode: string): Observable<Article> {
    return this.http.get<Article>(`${this.articleUrl}/${articleCode}`)
      .pipe(catchError(this.errorHandl));
  }

  public createArticle(article:Article): Observable<Article> {
    return this.http.post<Article>(this.articleUrl,article).pipe(catchError(this.errorHandl));
  }

  public deleteArticle(articleCode:string): Observable<unknown> {
    return this.http.delete(`${this.articleUrl}/${articleCode}`)
      .pipe(catchError(this.errorHandl));
  }

  public editArticle(articleCode:string, article:Article): Observable<Article> {
    return this.http.patch<Article>(`${this.articleUrl}/${articleCode}`, article)
      .pipe(catchError(this.errorHandl));
  }

  // Error handling
  errorHandl(error:any) {
    console.log(error);
    let errorMessage = '';
    if (error.status === 0 ) {
      // Get client-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.message}`;
    }
    window.alert(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }

}
