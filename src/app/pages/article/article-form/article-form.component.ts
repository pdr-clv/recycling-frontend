import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.scss']
})
export class ArticleFormComponent implements OnInit {

  articleCode: string | null = '';

  saveForm = this.formBuilder.group({
    articleCode: '',
    description: '',
    pricePurchase:'0.00',
    priceSale:'0.00',
    stock:'0.00'
  });

  constructor(
    private formBuilder: FormBuilder,
    private articleService: ArticleService,
    private router: Router,
    private activatedRoute:ActivatedRoute,
    private location: Location,
    ) { }

  ngOnInit(): void {
    this.articleCode = this.activatedRoute.snapshot.paramMap.get('articleCode');
    console.log('articleCode', this.articleCode);
    if (!!this.articleCode)
      this.articleService.getByCode(this.articleCode)
        .subscribe(data => this.saveForm.setValue(data));
  }

  onSave(): void {
    this.articleService.createArticle(this.saveForm.value).subscribe(data => {
      alert('Artículo guardado correctamente');
      this.router.navigate(["articles"]);
    });
  }

  onEdit(): void {
    if (!!this.articleCode)
      this.articleService.editArticle(this.articleCode,this.saveForm.value)
        .subscribe(data => {
          alert('Artículo modificado correctamente');
          this.router.navigate(["articles"]);
      });
  }

  onBack(): void {
    this.location.back();
  }

}
