import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from '../article.model';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent implements OnInit {

  articles: Article[] = [];
  displayedColumns: string[] = ['articleCode', 'description', 'pricePurchase', 'priceSale', 'stock', 'buttons'];
  searchInput: string = '';

  constructor(
    private articleService:ArticleService,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.articleService.getList().subscribe(data => {
      this.articles = data;
    });
  }

  onEdit(articleCode:string):void {
    this.router.navigate([`articles/form/${articleCode}`]);
  }

  onAdd():void {
    this.router.navigate(["articles/form"]);
  }

  onBuscar():void {
    this.articleService.getList(this.searchInput).subscribe(data => {
      this.articles = data;
    });
  }

  onReset():void {
    this.searchInput = '';
    this.articleService.getList().subscribe(data => {
      this.articles = data;
    });
  }

  onDelete(articleId:string): void {
    this.articleService.deleteArticle(articleId).subscribe( () => {
      this.articles = this.articles.filter((article:Article) => 
        article.articleCode !== articleId
      );
    });
    alert('Artículo eliminado');
  }
}
