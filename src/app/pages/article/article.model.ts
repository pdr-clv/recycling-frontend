export interface Article {
  articleCode: string;
  description: string;
  pricePurchase: number;
  priceSale: number;
  stock: number;
}
