import { NgModule } from '@angular/core';
import { ArticleListComponent } from './article-list/article-list.component';
import { SharedComponentsModule } from 'src/app/_shared/shared-components.module';
import { ArticleService } from './article.service';
import { ArticleFormComponent } from './article-form/article-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ArticleListComponent,
    ArticleFormComponent
  ],
  imports: [
    SharedComponentsModule,
    ReactiveFormsModule,

  ],
  providers: [ArticleService]
})
export class ArticleModule { }
